<?php
function nodeforum_views_default_views() {
  $view = new stdClass();
  $view->name = 'forum_replies';
  $view->description = 'A chronological listing of nodes.';
  $view->access = array (
);
  $view->view_args_php = '';
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => 'normal',
    ),
  );
  $view->argument = array (
    array (
      'type' => 'nodecomment_parent_nid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'moderate',
      'operator' => '=',
      'options' => '',
      'value' => '0',
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node);
  $view->page_type = 'teaser';
  $views[$view->name] = $view;

  return $views;
}
